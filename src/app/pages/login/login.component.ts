import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../_services/http.service';
import { AuthenticationService } from '../../_services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = {};

  constructor(
    private httpService: HttpService,
    private authenticationService: AuthenticationService,
    private router: Router
  ) { 
    console.log('Const Login');
  }

  ngOnInit() {
  }

  login() {
    console.log('logged', this.user);
    this.httpService.post('http://192.168.0.53:8081/login', this.user).subscribe(res => {
      if (res.success) {
        let result: any = {};
        result = res;
        this.authenticationService.login(result.data);
        this.router.navigate(['/app']);
      }
    }, error => {
      console.log('Error');
    });
  }

}
