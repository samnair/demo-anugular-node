import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { LocationStrategy, HashLocationStrategy, PathLocationStrategy } from '@angular/common';
import { HttpModule, Http } from '@angular/http';

import { AppConfig } from './app.config';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { SimpleLayoutComponent } from './layout/simple-layout/simple-layout.component';
import { PageNotFoundComponent } from './pages/common/page-not-found.component';
import { LocalStorageService } from './_services/local-storage.service';
import { AuthGuard } from './_services/auth.guard';
import { AuthLayoutComponent } from './layout/auth-layout/auth-layout.component';
import { HttpService } from './_services/http.service';
import { AuthenticationService } from './_services/authentication.service';
import { AuthModule } from './layout/auth-layout/auth.module';

export function initialConfigLoad(config: AppConfig) {
  return () => config.load();
}

@NgModule({
  declarations: [
    AppComponent,
    SimpleLayoutComponent,
    PageNotFoundComponent,
    AuthLayoutComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule, ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    AuthModule
  ],
  providers: [LocalStorageService, HttpService, AuthGuard, AuthenticationService, AppConfig,
  { provide: APP_INITIALIZER, useFactory: initialConfigLoad, deps: [AppConfig], multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
