import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from '../../_services/http.service';

@Component({
  selector: 'app-password-manager-form',
  templateUrl: './password-manager-form.component.html',
  styleUrls: ['./password-manager-form.component.css']
})
export class PasswordManagerFormComponent implements OnInit {
  // formData = {};
  formSubmitted = false;
  selectedData = {};
  @Input() formData: any;

  constructor(private httpService: HttpService) { }

  ngOnInit() {
    if (this.formData !== '') {
      console.log('edit');
    } else if (this.formData === '') {
      console.log('Add');
    }
  }

  saveForm() {
    this.formSubmitted = true;
    // /manage-password/save
    this.httpService.post('/manage-password/save', this.selectedData).subscribe(res => {
      if (res.success) {
        let result: any = {};
        result = res;
      }
    }, error => {
      console.log('Error');
    });
  }

}
