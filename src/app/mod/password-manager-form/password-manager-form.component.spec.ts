import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PasswordManagerFormComponent } from './password-manager-form.component';

describe('PasswordManagerFormComponent', () => {
  let component: PasswordManagerFormComponent;
  let fixture: ComponentFixture<PasswordManagerFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasswordManagerFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordManagerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
