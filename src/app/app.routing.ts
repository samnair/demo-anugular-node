import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Layout */
import { SimpleLayoutComponent } from './layout/simple-layout/simple-layout.component';
import { PageNotFoundComponent } from './pages/common/page-not-found.component';
import { AuthLayoutComponent } from './layout/auth-layout/auth-layout.component';
import { AuthGuard } from './_services/auth.guard';

export const routes: Routes = [

    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
    },
    {
        path: '',
        component: SimpleLayoutComponent,
        data: {
            title: 'Pages'
        },
        children: [
            {
                path: '',
                loadChildren: 'app/pages/pages.module#PagesModule',
            }
        ]
    },
    {
        path: 'app',
        canActivate: [AuthGuard],
        component: AuthLayoutComponent,
        data: {
            title: 'User - Pages'
        },
        children: [
            {
                path: '',
                loadChildren: 'app/layout/auth-layout/auth.module#AuthModule',
            }
        ]
    },
    {
        path: '403',
        component: PageNotFoundComponent
    },
    { path: '**', component: PageNotFoundComponent }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}
