import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

    loginKey = 'demo';

    constructor() { }
    getItem(key, json?: boolean) {
        if (json) {
            return JSON.parse(localStorage.getItem(this.loginKey + '_' + key));
        } else {
            return localStorage.getItem(this.loginKey + '_' + key);
        }

    }
    setItem(key, data) {
        // this.cookies.set(this.loginKey+'_'+key,data);
        localStorage.setItem(this.loginKey + '_' + key, data);
    }
    removeItem(key) {
        // this.cookies.delete(this.loginKey+'_'+key);
        localStorage.removeItem(this.loginKey + '_' + key);
    }
}
