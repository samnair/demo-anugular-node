import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { LocalStorageService } from './local-storage.service';
/* import {HttpService} from './http.service';
import {LocalStorageService} from './local-storage.service';
import {AuthenticationService} from './authentication.service'; */

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router,
                private localStorage: LocalStorageService ) {}

    canActivate() {
        console.log('Canavtivate');
        if (this.localStorage.getItem('currentUser')) {
            return true;
        } else {
            console.log('Not logged');
            this.router.navigate(['/login']);
            return false;
        }
    }
}

