import { Injectable, NgZone } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { HttpService } from './http.service';
import { LocalStorageService } from './local-storage.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Injectable()
export class AuthenticationService {
    constructor(private http: Http,
        private zone: NgZone,
        private router: Router,
        private httpService: HttpService,
        private location: Location,
        private localStorage: LocalStorageService) {
    }

    /* Used in -> login.component.ts */
    login(data) {
        console.log('data', data);
        this.localStorage.setItem('currentUser', JSON.stringify(data));
        return true;
    }

    logout() {
        // remove user from local storage to log user out
        this.localStorage.removeItem('currentUser');
        // this.localStorage.removeItem('currentMenu');
        // this.localStorage.removeItem('userMenu');

        this.httpService.get('logout').subscribe(
            data => {
                //  this.localStorage.removeItem('currentUser');
                //   this.localStorage.removeItem('currentMenu');
                // this.router.navigate(['/login']);
            },
            error => {
                //  this.localStorage.removeItem('currentUser');
                // this.localStorage.removeItem('currentMenu');
                // this.router.navigate(['/login']);
            });
        this.zone.runOutsideAngular(() => {
            // location.reload();
        });
        this.router.navigate(['/login']);
        // window.location.reload();

        return true;
    }
}
