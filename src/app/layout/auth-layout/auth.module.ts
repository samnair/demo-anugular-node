import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthRoutingModule } from './auth-routing.module';
import { PasswordManagerComponent } from '../../mod/password-manager/password-manager.component';
import { PasswordManagerFormComponent } from '../../mod/password-manager-form/password-manager-form.component';


@NgModule({
  declarations: [
    PasswordManagerComponent,
    PasswordManagerFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AuthRoutingModule
  ],
  providers: [],
  bootstrap: []
})
export class AuthModule { }
