import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PasswordManagerComponent } from '../../mod/password-manager/password-manager.component';


const routes: Routes = [
   {
    path: 'password-manager',
    component: PasswordManagerComponent,
    data: {
      title: 'Password Manager'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
