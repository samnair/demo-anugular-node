import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../_services/http.service';

@Component({
  selector: 'app-auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.css']
})
export class AuthLayoutComponent implements OnInit {

  constructor(private httpService: HttpService) { }

  ngOnInit() {
    // call menu
    this.httpService.get('assets/data/menu.json', {}).subscribe(res => {
      console.log('Menu', res);
    }, error => {
      console.log('Error');
    });
  }

}
